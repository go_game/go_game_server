use std::fs::File;
use std::io::Read;
use std::fmt::Debug;
use std::fmt;
#[derive(Clone, Copy, Serialize, Debug, PartialEq)]
pub enum Tile {
    Black,
    White,
    Empty,
}
#[derive(Serialize, Clone)]
pub struct Board {
    tiles: [[Tile; 19]; 19],
}


impl Board {
    pub fn new() -> Self {
        Board {
            tiles: [[Tile::Empty; 19]; 19],
        }
    }
    pub fn from_file(path: &str) -> Self {
        let mut board = Board {
            tiles: [[Tile::Empty; 19]; 19],
        };
        let mut f = File::open(path).expect("Input file oppening error");
        let mut input = String::new();
        f.read_to_string(&mut input).expect("Read from file error");
        for (i, line) in input.lines().enumerate() {
            let arr = line.trim().split_whitespace();
            let arr: Vec<u8> = arr.map(|item| item.parse::<u8>().unwrap()).collect();
            for (j, item) in arr.iter().enumerate() {
                match *item {
                    0 => board.tiles[i][j] = Tile::Empty,
                    1 => board.tiles[i][j] = Tile::White,
                    2 => board.tiles[i][j] = Tile::Black,
                    _ => panic!("Wrong input file"),
                }
            }
        }
        board
    }

    //Careful, unstable change i and j places(e.g workaround for some issues on client side)
    //currently abuses in game struct(remove_captured_stones)
    pub fn set_tile(&mut self, j: usize, i: usize, to_set: Tile) -> Result<(), String> {
        if i > 19 || j > 19 {
            return Err("Wrong position of tile to set".to_owned());
        }
        match self.tiles[i][j] {
            Tile::White | Tile::Black => Err("Tile is already used".to_owned()),
            _ => {
                self.tiles[i][j] = to_set;
                Ok(())
            }
        }
    }

    //temorary, TODO: refactor, add check mb
    pub fn set_tile_forced(&mut self, j: usize, i: usize, to_set: Tile) {
        self.tiles[i][j] = to_set;
    }

    pub fn get_tiles(&self) -> [[Tile; 19]; 19] {
        self.tiles
    }
}


impl Debug for Board {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for row in &self.tiles {
            for item in row {
                write!(
                    f,
                    "{} ",
                    match *item {
                        Tile::Empty => 0,
                        Tile::White => 1,
                        Tile::Black => 2,
                    }
                ).unwrap();
            }
            write!(f, "\n").unwrap();
        }
        write!(f, "")
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_tile_setting() {
        let mut b = Board::new();
        assert_eq!(
            b.set_tile(20, 2, Tile::Black),
            Err("Wrong position of tile to set")
        );
        assert_eq!(b.set_tile(1, 1, Tile::Black), Ok(()));
        assert_eq!(b.set_tile(1, 1, Tile::Black), Err("Tile is already used"));
    }

    #[test]
    fn test_creating_board_from_file() {
        let b = Board::from_file("test_data/board.dat");
        assert_eq!(b.tiles[1][1], Tile::White);
        assert_eq!(b.tiles[0][0], Tile::Empty);
        assert_eq!(b.tiles[3][7], Tile::Black);
        assert_eq!(b.tiles[17][17], Tile::White);
    }
}
