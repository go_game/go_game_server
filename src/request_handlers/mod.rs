use std::net::TcpStream;
use server::Server;
use cggp::request::{ConnectRequest, GetStatusRequest, MoveRequest};
use cggp::response::{ConnectResponse, GetStatusResponse, MoveResponse};
use game::GameTurn;
use serde_json;
use std::io::Write;
//temporary commented
fn response(stream: &mut TcpStream, serialized: &str) {
    //println!("Sending back: \n{}", serialized);
    stream.write_all(serialized.as_bytes()).unwrap();
    println!("Finished response");
}

pub fn handle_connect_request(
    stream: &mut TcpStream,
    server: &mut Server,
    request: &ConnectRequest,
) {
    let (connected, err) = server.check_for_av_game(&request.get_color(), &request.get_login());
    let serialized = serde_json::to_string(&ConnectResponse::new(connected, err))
        .expect("Failed to serialize ConnectResponse");
    response(stream, &serialized);
}

pub fn handle_get_status_request(
    stream: &mut TcpStream,
    server: &mut Server,
    request: &GetStatusRequest,
) {
    match server.get_game_clone_by_login(&request.get_login()) {
        Some(mut game) => {
            game.recalculate_score();
            let serialized = serde_json::to_string(&GetStatusResponse::new(&game)).unwrap();
            response(stream, &serialized);
        }
        None => unimplemented!(),
    }
}

pub fn handle_move_request(stream: &mut TcpStream, server: &mut Server, request: &MoveRequest) {

    let game = match server.get_game_by_login(&request.get_login()) {
        Ok(value) => value,
        Err(why) => {
            let serialized = serde_json::to_string(&MoveResponse::new(false, Some(why)))
                .expect("Failde to serialize MoveResopnse whith unfounded game");
            response(stream, &serialized);
            return;
        }
    };
    let color = game.get_player_color_by_login(&request.get_login())
        .expect("Failed to get color by login");

    let serialized =
        match game.make_turn(&GameTurn::new(color, request.get_x(), request.get_y())) {
            Ok(_) => {
                game.remove_captured_stones();
                game.recalculate_score();
                serde_json::to_string(&MoveResponse::new(true, None))
                    .expect("Failed to serialize valid move response")
            }
            Err(why) => {
                serde_json::to_string(&MoveResponse::new(false, Some(why)))
                    .expect("Failed to serialize invalid MoveResponse")
            }
        };
    println!("Sending back: {}", serialized);
    response(stream, &serialized);
}
