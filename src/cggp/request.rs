use player::Color;

pub enum Request {
    ConnectRequest(ConnectRequest),
    DisconnectRequest(DisconnectRequest),
    MoveRequest(MoveRequest),
    PassRequest(PassRequest),
    GetStatusRequest(GetStatusRequest),
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ConnectRequest {
    login: String,
    color: Color,
}

impl ConnectRequest {
    pub fn new(login: &str, color: Color) -> Self {
        ConnectRequest {
            login: login.to_owned(),
            color,
        }
    }
    pub fn get_color(&self) -> Color {
        self.color
    }
    pub fn get_login(&self) -> String {
        self.login.clone()
    }
}

pub struct DisconnectRequest {
    login: String,
}
#[derive(Deserialize)]
pub struct MoveRequest {
    login: String,
    x: u8,
    y: u8,
}

impl MoveRequest {
    pub fn new(login: &str, x: u8, y: u8) -> Self {
        MoveRequest {
            login: login.to_owned(),
            x,
            y,
        }
    }

    pub fn get_login(&self) -> String {
        self.login.clone()
    }

    pub fn get_x(&self) -> u8 {
        self.x
    }

    pub fn get_y(&self) -> u8 {
        self.y
    }
}


#[derive(Debug)]
pub struct PassRequest {
    login: String,
}

impl PassRequest {
    pub fn new(login: &str) -> Self {
        PassRequest {
            login: (*login).to_string(),
        }
    }
}
#[derive(Deserialize)]
pub struct GetStatusRequest {
    login: String,
}

impl GetStatusRequest {
    pub fn new(login: &str) -> Self {
        GetStatusRequest {
            login: login.to_owned(),
        }
    }
    pub fn get_login(&self) -> String {
        self.login.clone()
    }
}
