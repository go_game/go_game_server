use game::Game;

#[derive(Serialize, Deserialize, Debug)]
pub struct ConnectResponse {
    successful: bool,
    err: Option<String>,
}

impl ConnectResponse {
    pub fn new(successful: bool, err: Option<String>) -> Self {
        ConnectResponse { successful, err }
    }
}

pub struct DisconnectResponse {
    successful: bool,
    err: Option<u8>,
}
#[derive(Serialize)]
pub struct MoveResponse {
    valid: bool,
    err: Option<String>,
}

impl MoveResponse {
    pub fn new(valid: bool, err: Option<String>) -> Self {
        MoveResponse { valid, err }
    }
}

pub struct PassResponse {
    successful: bool,
    err: Option<u8>,
}
#[derive(Serialize)]
pub struct GetStatusResponse {
    status: Game,
}

impl GetStatusResponse {
    pub fn new(status: &Game) -> Self {
        GetStatusResponse {
            status: status.clone(),
        }
    }
}
