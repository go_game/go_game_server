use board::Board;
use board::Tile;
use player::{Player, Color};
use std::fmt;
#[derive(Serialize, Clone)]
pub struct Game {
    turn: usize,
    board: Board,
    white_player: Player,
    black_player: Player,
}

pub struct GameTurn {
    player_color: Color,
    x: u8,
    y: u8,
}


impl GameTurn {
    pub fn new(player_color: Color, x: u8, y: u8) -> Self {
        GameTurn { player_color, x, y }
    }
}


struct Territory {
    territory: Vec<Vec<usize>>,
}
impl Territory {
    pub fn new() -> Self {
        Territory {
            territory: vec![vec![0; 19]; 19],
        }
    }

    fn cancer(
        &mut self,
        tiles: &[[Tile; 19]; 19],
        x: usize,
        y: usize,
        id: usize,
        finded: &mut (bool, bool),
    ) {
        if self.territory[x][y] == id {
            return;
        }
        if tiles[x][y] == Tile::Black {
            finded.0 = true;
            return;
        }
        if tiles[x][y] == Tile::White {
            finded.1 = true;
            return;
        }
        self.territory[x][y] = id;
        let x: i32 = x as i32;
        let y: i32 = y as i32;
        self.cancer(tiles, (x - 1).abs() as usize, y as usize, id, finded);
        if x <= 17 {
            self.cancer(tiles, (x + 1) as usize, y as usize, id, finded);
        }
        self.cancer(tiles, x as usize, (y - 1).abs() as usize, id, finded);
        if y <= 17 {
            self.cancer(tiles, x as usize, (y + 1) as usize, id, finded);
        }
    }

    fn cancer_for_freedom(
        &mut self,
        tiles: &[[Tile; 19]; 19],
        x: usize,
        y: usize,
        id: usize,
        cap: &mut (bool, bool),
        t_color: &Tile,
    ) {
        if self.territory[x][y] == id {
            return;
        }
        if tiles[x][y] == Tile::Empty {
            cap.0 = false;
            return;
        }
        if tiles[x][y] == *t_color {
            return;
        }
        self.territory[x][y] = id;
        let x: i32 = x as i32;
        let y: i32 = y as i32;
        self.cancer_for_freedom(tiles, (x - 1).abs() as usize, y as usize, id, cap, t_color);
        if x <= 17 {
            self.cancer_for_freedom(tiles, (x + 1) as usize, y as usize, id, cap, t_color);
        }
        self.cancer_for_freedom(tiles, x as usize, (y - 1).abs() as usize, id, cap, t_color);
        if y <= 17 {
            self.cancer_for_freedom(tiles, x as usize, (y + 1) as usize, id, cap, t_color);
        }
    }
}

impl Game {
    pub fn new() -> Self {
        Game {
            turn: 0,
            board: Board::new(),
            white_player: Player::new_white(),
            black_player: Player::new_black(),
        }
    }

    fn check_who_should_make_move(&self) -> Color {
        match self.turn & 1 {
            1 => Color::White,
            _ => Color::Black,
        }
    }

    pub fn recalculate_score(&mut self) {
        let mut black_ids = Vec::new();
        let mut white_ids = Vec::new();
        let board = self.board.get_tiles();
        let mut id_field = Territory::new();
        for i in 0..board.len() {
            for j in 0..board.len() {
                if board[i][j] == Tile::Empty && id_field.territory[i][j] == 0 {
                    let id = i * 19 + j + 1;
                    let mut finded = (false, false);
                    id_field.cancer(&board, i, j, id, &mut finded);
                    if finded.0 == true && finded.1 == false {
                        black_ids.push(id);
                    } else if finded.0 == false && finded.1 == true {
                        white_ids.push(id);
                    }
                }
            }
        }

        let mut white_player_score = 0;
        let mut black_player_score = 0;
        for i in 0..board.len() {
            for j in 0..board.len() {
                if white_ids.contains(&id_field.territory[i][j]) {
                    white_player_score += 1;
                } else if black_ids.contains(&id_field.territory[i][j]) {
                    black_player_score += 1;
                }
            }
        }
        self.black_player.set_score(black_player_score);
        self.white_player.set_score(white_player_score);
    }

    pub fn remove_captured_stones(&mut self) {
        let mut black_stones_to_remove = Vec::new();
        let mut white_stones_to_remove = Vec::new();
        let board = self.board.get_tiles();
        let mut id_field = Territory::new();
        for i in 0..board.len() {
            for j in 0..board.len() {
                if (board[i][j] == Tile::White || board[i][j] == Tile::Black) &&
                    id_field.territory[i][j] == 0
                {
                    let id = i * 19 + j + 1;
                    let mut captured = (true, true);
                    let tile = match board[i][j] {
                        Tile::White => Tile::Black,
                        _ => Tile::White,
                    };
                    id_field.cancer_for_freedom(&board, i, j, id, &mut captured, &tile);
                    if captured.0 {
                        match board[i][j] {
                            Tile::Black => black_stones_to_remove.push(id),
                            Tile::White => white_stones_to_remove.push(id),
                            _ => unreachable!(),
                        }
                    }
                }
            }
        }
        let mut white_score_to_remove = 0;
        let mut black_score_to_remove = 0;
        //TODO fix indexes
        for i in 0..board.len() {
            for j in 0..board.len() {
                if black_stones_to_remove.contains(&id_field.territory[i][j]) {
                    self.board.set_tile_forced(j, i, Tile::Empty);
                    black_score_to_remove += 1;
                } else if white_stones_to_remove.contains(&id_field.territory[i][j]) {
                    self.board.set_tile_forced(j, i, Tile::Empty);
                    white_score_to_remove += 1;
                }
            }
        }
        self.white_player.increase_penalty_by(white_score_to_remove);
        self.black_player.increase_penalty_by(black_score_to_remove);
    }

    pub fn make_turn(&mut self, turn: &GameTurn) -> Result<(), String> {
        if self.check_who_should_make_move() == turn.player_color {
            self.turn += 1;
            self.board.set_tile(
                turn.x.into(),
                turn.y.into(),
                match turn.player_color {
                    Color::Black => Tile::Black,
                    Color::White => Tile::White,
                },
            )
        } else {
            Err("Not your turn".to_owned())
        }
    }

    pub fn get_player_conenction_status(&self, color: &Color) -> bool {
        match *color {
            Color::White => self.white_player.get_connection_status(),
            Color::Black => self.black_player.get_connection_status(),
        }
    }
    pub fn connect_player(&mut self, color: &Color, login: &str) {
        match *color {
            Color::White => self.white_player.connect(login),
            Color::Black => self.black_player.connect(login),
        }
    }

    pub fn get_player_login(&self, color: &Color) -> Option<String> {
        match *color {
            Color::White => self.white_player.get_login(),
            Color::Black => self.black_player.get_login(),
        }
    }


    pub fn player_already_connected(&self, login: &str) -> bool {
        self.white_player.get_login() == Some(login.to_owned()) ||
            self.black_player.get_login() == Some(login.to_owned())
    }

    pub fn free_color_slot(&self, color: &Color) -> bool {
        !self.get_player_conenction_status(color)
    }

    pub fn get_player_color_by_login(&self, login: &str) -> Option<Color> {
        if Some(login.to_owned()) == self.white_player.get_login() {
            Some(Color::White)
        } else if Some(login.to_owned()) == self.black_player.get_login() {
            Some(Color::Black)
        } else {
            None
        }
    }
}

impl fmt::Debug for Game {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Board: \n{:?}", self.board).unwrap();
        writeln!(
            f,
            "First player has {} stones left and earned {} points, connection status: {}",
            self.white_player.get_stones_left(),
            self.white_player.get_score(),
            self.white_player.get_connection_status()
        ).unwrap();
        writeln!(
            f,
            "Second player has {} stones left and earned {} points, connection status: {}",
            self.black_player.get_stones_left(),
            self.black_player.get_score(),
            self.black_player.get_connection_status()
        ).unwrap();
        writeln!(f, "Current turn is {}", self.turn)
    }
}
