use std::net::{TcpListener, TcpStream};
use std::io::{BufReader, BufRead, Error};
use player::Color;
use cggp::request::*;
use request_parsers;
use game::Game;
use request_handlers::*;

#[derive(Debug)]
pub struct Server {
    current_games: Vec<Game>,
}

fn read_incoming_data(stream: &mut TcpStream) -> Option<String> {
    let mut input = String::new();
    let mut reader = BufReader::new(stream.try_clone().expect(&format!(
        "Error during try_clone() in {}: {}",
        file!(),
        line!()
    )));
    match reader.read_line(&mut input) {
        Ok(_) => Some(input),
        Err(_) => None,
    }
}


fn parse_request(request_as_string: &str) -> Option<Request> {
    println!("At parse_request state: {}", request_as_string);
    match request_as_string.split_whitespace().next() {
        None => None,
        Some("Con") => Some(request_parsers::parse_connect_request(request_as_string)),
        Some("GetStatus") => Some(request_parsers::parse_get_status_request(request_as_string)),
        Some("Move") => Some(request_parsers::parse_move_request(request_as_string)),
        Some(value) => {
            println!("Strange message type: {:?}", value);
            unimplemented!()
        }
    }
}


fn handle_request(server: &mut Server, stream: &mut TcpStream) {
    let request = match read_incoming_data(stream) {
        Some(value) => value,
        None => {
            println!("Error during read from stream");
            return;
        }
    };
    println!("Recieved {:?}", request);
    let incoming_request = match parse_request(&request) {
        Some(request) => request,
        None => return,
    };
    match incoming_request {
        Request::ConnectRequest(request) => handle_connect_request(stream, server, &request),
        Request::GetStatusRequest(request) => handle_get_status_request(stream, server, &request),
        Request::MoveRequest(request) => handle_move_request(stream, server, &request),
        _ => unimplemented!(),
    }
}

impl Server {
    pub fn new() -> Self {
        Server {
            current_games: vec![Game::new()],
        }
    }


    pub fn start_listen(&mut self) -> Result<(), Error> {
        let listener = TcpListener::bind("127.0.0.1:32444").unwrap();
        for stream in listener.incoming() {
            match stream {
                Ok(mut stream) => {
                    handle_request(self, &mut stream);
                }
                Err(why) => return Err(why),
            }
        }
        Ok(())
    }
    pub fn check_for_av_game(&mut self, color: &Color, login: &str) -> (bool, Option<String>) {
        for mut game in &mut self.current_games {
            if game.player_already_connected(login) {
                return (false, Some("Already connected".to_owned()));
            }
            if game.free_color_slot(color) {
                game.connect_player(color, login);
                return (true, None);
            }
        }
        (false, Some("No free games".to_owned()))
    }

    pub fn get_game_clone_by_login(&self, login: &str) -> Option<Game> {
        for game in &self.current_games {
            if (*game).get_player_login(&Color::White) == Some(login.to_owned()) ||
                (*game).get_player_login(&Color::Black) == Some(login.to_owned())
            {
                return Some(game.clone());
            }
        }
        None
    }

    pub fn get_game_by_login(&mut self, login: &str) -> Result<&mut Game, String> {
        for game in &mut self.current_games {
            if (*game).get_player_login(&Color::White) == Some(login.to_owned()) ||
                (*game).get_player_login(&Color::Black) == Some(login.to_owned())
            {
                return Ok(game);
            }
        }
        Err("Can't find game by login".to_owned())
    }
}
