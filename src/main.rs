// #![allow(dead_code)]
#[macro_use]
extern crate serde_derive;

extern crate serde;
extern crate serde_json;
mod server;
mod board;
mod player;
mod game;
mod cggp;
mod request_parsers;
mod request_handlers;
use server::Server;


fn main() {
    let mut s = Server::new();
    s.start_listen().unwrap();
}
