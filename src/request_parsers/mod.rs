use cggp::request::*;
use serde_json;


pub fn parse_connect_request(request: &str) -> Request {
    let (_, request) = request.split_at(4);
    let ret: ConnectRequest =
        serde_json::from_str(request).expect("Failed to deserialize ConnectRequest");
    Request::ConnectRequest(ConnectRequest::new(&ret.get_login(), ret.get_color()))
}

pub fn parse_get_status_request(request: &str) -> Request {
    let (_, request) = request.split_at(9);
    let ret: GetStatusRequest =
        serde_json::from_str(request).expect("Failed to deserialize GetStatusRequest");
    Request::GetStatusRequest(GetStatusRequest::new(&ret.get_login()))
}

pub fn parse_move_request(request: &str) -> Request {
    let (_, request) = request.split_at(5);
    let ret: MoveRequest =
        serde_json::from_str(request).expect("Failed to deserialize MoveRequest");
    Request::MoveRequest(MoveRequest::new(&ret.get_login(), ret.get_x(), ret.get_y()))
}
