#[derive(Debug, Serialize, Deserialize, Clone, Copy, PartialEq)]
pub enum Color {
    White,
    Black,
}
#[derive(Debug, Serialize, Clone)]
pub struct Player {
    color: Color,
    stones_left: usize,
    score: usize,
    connected: bool,
    login: Option<String>,
    penalty: usize,
}

impl Player {
    pub fn new_white() -> Self {
        Player {
            color: Color::White,
            stones_left: 180,
            score: 0,
            connected: false,
            login: None,
            penalty: 0,
        }
    }
    pub fn new_black() -> Self {
        Player {
            color: Color::Black,
            stones_left: 181,
            score: 0,
            connected: false,
            login: None,
            penalty: 0,
        }
    }
    pub fn set_score(&mut self, score: usize) {
        self.score = score;
    }
    pub fn get_score(&self) -> usize {
        self.score
    }

    pub fn increase_penalty_by(&mut self, increment: usize) {
        self.penalty += increment;
    }
    pub fn get_stones_left(&self) -> usize {
        self.stones_left
    }
    pub fn get_connection_status(&self) -> bool {
        self.connected
    }
    pub fn connect(&mut self, login: &str) {
        self.connected = true;
        self.login = Some((*login).to_string());
    }
    pub fn get_login(&self) -> Option<String> {
        self.login.clone()
    }
}
